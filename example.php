<?php

require 'vendor/autoload.php';

use TLink\PointService;

$secret_key = 'your-secret-key';
$code = 'giftcardcode';

$service = new PointService($secret_key);
$email  = 'user@email.com';

try {

    // Get points for user
    $points = $service->getForUser($email);

} catch (\Exception $e) {
    echo json_decode($e->getMessage())->error;
    exit;
}

echo 'User: ' . $email . ' have ' . $points . ' points';
echo '<br>';


try {

    // Add points to user's account
    $points = $service->addToUser($email, 50, 'Refund from shop');

} catch (\Exception $e) {
    echo json_decode($e->getMessage())->error;
    exit;
}

echo 'User: ' . $email . ' have ' . $points . ' points';
echo '<br>';

try {

    // Remove points from user's account
    $points = $service->removeFromUser($email, 50, 'Purchase from shop');

} catch (\Exception $e) {
    echo json_decode($e->getMessage())->error;
    exit;
}

echo 'User: ' . $email . ' have ' . $points . ' points';
echo '<br>';

try {

    // Redeem coupon code
    $coupon = $service->getCoupon($code);

} catch (\Exception $e) {
    echo json_decode($e->getMessage())->error;
    exit;
}

echo 'Coupon with code: ' . $code . ' have ' . $coupon->gift_card->amount . ' points';
echo '<br>';

try {

    // Redeem coupon code
    $points = $service->redeemCoupon($email, $code);

} catch (\Exception $e) {
    echo json_decode($e->getMessage())->error;
    exit;
}

echo 'User: ' . $email . ' have ' . $points . ' points';
echo '<br>';

try {

    // Get sorted points transaction list
    $transactions = $service->getTransactions($email);

} catch (\Exception $e) {
    echo json_decode($e->getMessage())->error;
    exit;
}

echo 'Transaction history: ';
echo '<pre>';
print_r($transactions);