<?php namespace TLink\Auth;

class AuthService {

    /**
     * Generate authentication hash
     *
     * @param string $secretKey
     * @param string $salt
     * @return array
     */
    public static function makeHash($secretKey, $salt = 'tlink-web-shop') {

        $time = time();

        return [
            'time'  => $time,
            'hash'  => sha1($time . $salt . $secretKey)
        ];

    }

}