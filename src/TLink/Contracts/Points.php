<?php namespace TLink\Contracts;

interface Points {

    /**
     * Get points for requested user's email
     *
     * @param string $email
     * @return float
     * @throws \Exception
     */
    public function getForUser($email);

    /**
     * Add points to user
     *
     * @param string $email
     * @param float|int $points
     * @param string $comment
     * @return mixed
     * @throws \Exception
     */
    public function addToUser($email, $points, $comment);

    /**
     * Remove points from user
     *
     * @param string $email
     * @param float|int $points
     * @param string $comment
     * @return mixed
     * @throws \Exception
     */
    public function removeFromUser($email, $points, $comment);

    /**
     * Get transaction history
     *
     * @param string $email
     * @return mixed
     * @throws \Exception
     */
    public function getTransactions($email);

}