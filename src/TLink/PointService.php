<?php namespace TLink;

use GuzzleHttp\Exception\RequestException;
use TLink\Auth\AuthService;
use TLink\Contracts\Points;
use GuzzleHttp\Client;

class PointService implements Points {

    protected $secret;
    protected $client;
    protected $endpoint = 'https://tournaments.golftlink.com/api/service';

    /**
     * PointService constructor.
     *
     * @param $secret
     * @param bool $endpoint
     * @throws \Exception
     */
    public function __construct($secret, $endpoint = false) {

        if( is_null($secret)) throw new \Exception('Secret key is missing');

        $this->secret = $secret;

        if($endpoint) $this->endpoint = $endpoint;

        $this->client = new Client();

    }

    /**
     * Get points for requested user's email
     *
     * @param string $email
     * @return float
     * @throws \Exception
     */
    public function getForUser($email) {

        try {

            $response = $this->client->get($this->endpoint . '/points?user=' . $email, ['headers' => $this->makeAuthHeaders()]);
            $response = json_decode($response->getBody()->getContents());

        } catch (RequestException $e) {
            throw new \Exception($e->getResponse()->getBody()->getContents());
        }

        return $response->points;

    }

    protected function makeAuthHeaders() {

        return AuthService::makeHash($this->secret);

    }

    /**
     * Add points to user
     *
     * @param string $email
     * @param float|int $points
     * @param string $comment
     * @return mixed
     * @throws \Exception
     */
    public function addToUser($email, $points, $comment) {

        try {

            $response = $this->client->post($this->endpoint . '/points/add', [
                'headers' => $this->makeAuthHeaders(),
                'json' => [
                    'user' => $email, 'points' => $points, 'comment' => $comment
                ]
            ]);

            $response = json_decode($response->getBody()->getContents());

        } catch (RequestException $e) {
            throw new \Exception($e->getResponse()->getBody()->getContents());
        }

        return $response->points;

    }

    /**
     * Remove points from user
     *
     * @param string $email
     * @param float|int $points
     * @param string $comment
     * @return mixed
     * @throws \Exception
     */
    public function removeFromUser($email, $points, $comment) {

        try {

            $response = $this->client->post($this->endpoint . '/points/subtract', [
                'headers' => $this->makeAuthHeaders(),
                'json' => [
                    'user' => $email, 'points' => $points, 'comment' => $comment
                ]
            ]);

            $response = json_decode($response->getBody()->getContents());

        } catch (RequestException $e) {
            throw new \Exception($e->getResponse()->getBody()->getContents());
        }

        return $response->points;

    }

    /**
     * Get transaction history
     *
     * @param string $email
     * @return mixed
     * @throws \Exception
     */
    public function getTransactions($email) {

        try {

            $response = $this->client->get($this->endpoint . '/points/changes?user=' . $email, ['headers' => $this->makeAuthHeaders()]);
            $response = json_decode($response->getBody()->getContents());

        } catch (RequestException $e) {
            throw new \Exception($e->getResponse()->getBody()->getContents());
        }

        return $response->transactions;

    }

    /**
     * Redeem coupon
     *
     * @param $email
     * @param $coupon
     * @return mixed
     * @throws \Exception
     */
    public function redeemCoupon($email, $coupon) {

        try {

            $response = $this->client->post($this->endpoint . '/coupon/redeem', [
                'headers' => $this->makeAuthHeaders(),
                'json' => [
                    'user' => $email, 'code' => $coupon
                ]
            ]);

            $response = json_decode($response->getBody()->getContents());

        } catch (RequestException $e) {
            throw new \Exception($e->getResponse()->getBody()->getContents());
        }

        return $response->points;

    }


    /**
     * Get coupon by code
     *
     * @param $code
     * @return mixed
     * @throws \Exception
     */
    public function getCoupon($code) {

        try {

            $response = $this->client->get($this->endpoint . '/coupon/' . $code, [
                'headers' => $this->makeAuthHeaders()
            ]);

            $response = json_decode($response->getBody()->getContents());

        } catch(RequestException $e) {
            throw new \Exception($e->getResponse()->getBody()->getContents());
        }

        return $response->code;

    }

}